using System;
using Android.App;
using Android.Content;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.OS;
using Java.Lang;
using Android.Graphics;
using Android.Support.V4.App;
using Android.Media;
using Android.Util;
using Android.Content.Res;
using Java.IO;
using System.IO;
using Android;

namespace ServiceAndNotification
{
    class MyNotification : Java.Lang.Object
    {
        public const int SIMPLE_NOTIFICATION_ID = 0;
        public const int CUSTOM_NOTIFICATION_ID = 1;
        public const string ACTION_INTENT = "ACTION INTENT";
        //private Button startNotification = null;
        //private Button startCustomNotification = null;
        private PendingIntent pendingIntent = null;
        private PendingIntent actionIntent = null;
        private Android.Net.Uri sound = null;
        private int lightsOn = 1000;
        private int lightsOff = 5000;
        private long[] vibratePattern = { 100, 200, 300 };
        private Bitmap bitmap = null;
        private Intent intent = null;
        private Context context = null;
        private Notification notification = null;
        private NotificationManagerCompat notificationManagerCompat = null;

        private static Handler mHandler = new Handler();
        public const int TEN_SECONDS = 10000;

        public MyNotification(Intent intent, Context context)
        {
            this.intent = intent;
            this.context = context;
        }

        public bool IsAndroidVersionJellyBeanOrNewer()
        {
            Android.OS.BuildVersionCodes currentApiVersion = Android.OS.Build.VERSION.SdkInt;
            if (currentApiVersion >= Android.OS.BuildVersionCodes.JellyBean)
            {
                return true;
            }

            return false;
        }

        public void CreateNotification()
        {
            if (IsAndroidVersionJellyBeanOrNewer())
            {
                Android.App.TaskStackBuilder taskStackBuilder = Android.App.TaskStackBuilder.Create(context);
                taskStackBuilder.AddParentStack(Class.FromType(typeof(PendingIntentActivity)));
                taskStackBuilder.AddNextIntent(intent);

                pendingIntent = taskStackBuilder.GetPendingIntent(SIMPLE_NOTIFICATION_ID, PendingIntentFlags.UpdateCurrent);
                actionIntent = PendingIntent.GetBroadcast(context, 0, new Intent(ACTION_INTENT), 0);
            }

            string soundFile = "tone";//"sound";
            sound = Android.Net.Uri.Parse("android.resource://" + context.ApplicationContext.PackageName + "/raw/" + soundFile);

            bitmap = BitmapFactory.DecodeResource(context.ApplicationContext.Resources, Resource.Drawable.large_icon);

            NotificationCompat.InboxStyle inboxStyle = new NotificationCompat.InboxStyle();
            inboxStyle.SetBigContentTitle("BIG CONTENT TITLE");
            inboxStyle.SetSummaryText("SUMMARY TEXT");
            inboxStyle.AddLine("LINE 1");
            inboxStyle.AddLine("LINE 2");
            inboxStyle.AddLine("LINE 3");

            NotificationCompat.Builder builder = new NotificationCompat.Builder(context);
            builder.SetSmallIcon(Resource.Drawable.Icon);
            builder.SetContentTitle("CONTENT TITLE");
            builder.SetContentText("CONTENT TEXT");
            builder.SetTicker("TICKER");
            builder.SetWhen(Java.Lang.JavaSystem.CurrentTimeMillis());
            builder.SetAutoCancel(true);
            builder.SetOngoing(false);
            builder.SetLights(Color.Red, lightsOn, lightsOff);
            builder.SetSound(sound);
            builder.SetVibrate(vibratePattern);

            if (IsAndroidVersionJellyBeanOrNewer())
            {
                builder.SetContentIntent(pendingIntent);
                builder.SetDeleteIntent(pendingIntent);
                builder.AddAction(Resource.Drawable.red_icon, "ACTION INTENT", actionIntent);
            }

            builder.SetContentInfo("CONTENT INFO");
            builder.SetNumber(123);
            builder.SetSubText("SUBTEXT");
            //builder.SetLargeIcon(bitmap);//TODO ver tamanho do bitmap para n�o dar erro de falta de mem�ria
            builder.SetStyle(inboxStyle);

            //NotificationManagerCompat notificationManagerCompat = NotificationManagerCompat.From(context);
            //notificationManagerCompat.Notify(SIMPLE_NOTIFICATION_ID, builder.Build());

            notificationManagerCompat = NotificationManagerCompat.From(context);
            notification = builder.Build();
        }

        public void StartNotification(int sleep)
        {
            if(mHandler != null && context != null && notificationManagerCompat != null && notification != null) {
                try
                {                    
                    Handler handler = new Handler(Looper.MainLooper);
                    handler.Post(() =>
                    {
                        notificationManagerCompat.Notify(SIMPLE_NOTIFICATION_ID, notification);
                    });

                    Thread.Sleep(sleep);
                }
                catch (System.Exception e)
                {
                    Log.Debug("TAG", e.ToString());
                }
            }            
        }
    }
}