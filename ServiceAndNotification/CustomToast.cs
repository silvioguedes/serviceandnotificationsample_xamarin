using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.Util;

namespace ServiceAndNotification
{
    public class CustomToast
    {        
        public const int TEN_SECONDS = 10000;

        public static void ShowToastByHandler(Context context, string text, int sleep)
        {
            try
            {
                Thread.Sleep(sleep);
                Handler handler = new Handler(Looper.MainLooper);
                handler.Post(() =>
                {
                    Toast.MakeText(context, text, ToastLength.Long).Show();
                });               
             }
             catch (Exception e)
             {
                Log.Debug("TAG", e.ToString());
             }
        }
    }
}