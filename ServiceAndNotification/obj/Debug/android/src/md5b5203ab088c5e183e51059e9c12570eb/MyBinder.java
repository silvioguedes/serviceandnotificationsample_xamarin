package md5b5203ab088c5e183e51059e9c12570eb;


public class MyBinder
	extends android.os.Binder
	implements
		mono.android.IGCUserPeer
{
/** @hide */
	public static final String __md_methods;
	static {
		__md_methods = 
			"";
		mono.android.Runtime.register ("ServiceAndNotification.MyBinder, ServiceAndNotification, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null", MyBinder.class, __md_methods);
	}


	public MyBinder () throws java.lang.Throwable
	{
		super ();
		if (getClass () == MyBinder.class)
			mono.android.TypeManager.Activate ("ServiceAndNotification.MyBinder, ServiceAndNotification, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null", "", this, new java.lang.Object[] {  });
	}

	public MyBinder (md5b5203ab088c5e183e51059e9c12570eb.MyService p0) throws java.lang.Throwable
	{
		super ();
		if (getClass () == MyBinder.class)
			mono.android.TypeManager.Activate ("ServiceAndNotification.MyBinder, ServiceAndNotification, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null", "ServiceAndNotification.MyService, ServiceAndNotification, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null", this, new java.lang.Object[] { p0 });
	}

	private java.util.ArrayList refList;
	public void monodroidAddReference (java.lang.Object obj)
	{
		if (refList == null)
			refList = new java.util.ArrayList ();
		refList.add (obj);
	}

	public void monodroidClearReferences ()
	{
		if (refList != null)
			refList.clear ();
	}
}
