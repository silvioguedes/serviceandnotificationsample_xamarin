package md5b5203ab088c5e183e51059e9c12570eb;


public class MyNotification
	extends java.lang.Object
	implements
		mono.android.IGCUserPeer
{
/** @hide */
	public static final String __md_methods;
	static {
		__md_methods = 
			"";
		mono.android.Runtime.register ("ServiceAndNotification.MyNotification, ServiceAndNotification, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null", MyNotification.class, __md_methods);
	}


	public MyNotification () throws java.lang.Throwable
	{
		super ();
		if (getClass () == MyNotification.class)
			mono.android.TypeManager.Activate ("ServiceAndNotification.MyNotification, ServiceAndNotification, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null", "", this, new java.lang.Object[] {  });
	}

	public MyNotification (android.content.Intent p0, android.content.Context p1) throws java.lang.Throwable
	{
		super ();
		if (getClass () == MyNotification.class)
			mono.android.TypeManager.Activate ("ServiceAndNotification.MyNotification, ServiceAndNotification, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null", "Android.Content.Intent, Mono.Android, Version=0.0.0.0, Culture=neutral, PublicKeyToken=84e04ff9cfb79065:Android.Content.Context, Mono.Android, Version=0.0.0.0, Culture=neutral, PublicKeyToken=84e04ff9cfb79065", this, new java.lang.Object[] { p0, p1 });
	}

	private java.util.ArrayList refList;
	public void monodroidAddReference (java.lang.Object obj)
	{
		if (refList == null)
			refList = new java.util.ArrayList ();
		refList.add (obj);
	}

	public void monodroidClearReferences ()
	{
		if (refList != null)
			refList.clear ();
	}
}
