using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.Util;

namespace ServiceAndNotification
{
    public class MyBinder : Binder
    {
        private MyService service = null;

        public MyBinder(MyService service)
        {
            this.service = service;            
        }

        public MyService GetService()
        {            
            return service;
        }
    }
}