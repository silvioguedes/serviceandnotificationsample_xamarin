using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using System.Threading;
using Android.Util;

namespace ServiceAndNotification
{
    [Service]
    public class MyService :Service
    {
        private bool stopThread = false;
        public const String STOP_THREAD = "stopThread";
        private Thread thread = null;

        private MyNotification notification = null;

        public MyService()
        {
            
        }

        public override void OnCreate(){
            base.OnCreate();

            CustomToast.ShowToastByHandler(ApplicationContext, "SERVICE STARTED", 0);

            stopThread = false;

            Intent intent = new Intent(this, typeof(PendingIntentActivity));
            notification = new MyNotification(intent,ApplicationContext);
            notification.CreateNotification();
        }

        public override StartCommandResult OnStartCommand(Intent intent, StartCommandFlags flags, int startID)
        {            
            stopThread = intent.GetBooleanExtra(STOP_THREAD, false);

            thread = new Thread(() =>
            {
                while (stopThread == false)
                {
                    if (notification != null)
                    {
                        notification.StartNotification(MyNotification.TEN_SECONDS);
                    }
                }

                if (stopThread)
                {
                    //TODO
                }

            });
            thread.Start();
            
            return base.OnStartCommand(intent, flags, startID);
        }

        public override IBinder OnBind(Intent intent)
        {
            if (intent != null)
            {
                CustomToast.ShowToastByHandler(ApplicationContext, "SERVICE BOUND", 0);
            }
            
            MyBinder binder = new MyBinder(this);

            return binder;
        }

        public override void OnDestroy()
        {
            stopThread = true;

            if (thread != null)
            {
                thread = null;
            }

            CustomToast.ShowToastByHandler(ApplicationContext, "SERVICE STOPPED", 0);

            base.OnDestroy();            
        }

         
        public override bool OnUnbind(Intent intent) {
            CustomToast.ShowToastByHandler(ApplicationContext, "SERVICE UNBOUND", 0);

            return false;
        }
    }
}