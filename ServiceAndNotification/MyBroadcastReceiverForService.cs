using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.Util;

namespace ServiceAndNotification
{
    [BroadcastReceiver]
    [IntentFilter(new String[] { "android.intent.action.BOOT_COMPLETED" })]
    public class MyBroadcastReceiverForService : BroadcastReceiver
    {
        public MyBroadcastReceiverForService()
        {
            
        }

        public override void OnReceive(Context context, Intent intent)
        {                        
            Intent newIntent = new Intent(context,typeof(MyService));
            newIntent.PutExtra(MyService.STOP_THREAD,false);
            context.StartService(newIntent);
        }
    }
}