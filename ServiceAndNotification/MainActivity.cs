﻿using System;
using Android.App;
using Android.Content;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.OS;

namespace ServiceAndNotification
{
    [Activity(Label = "ServiceAndNotification", MainLauncher = true, Icon = "@drawable/icon")]
    public class MainActivity : Activity
    {
        private Button startServiceButton = null;
        private Button stopServiceButton = null;

        private MyServiceConnection serviceConnection = null;
        private Intent serviceIntent = null;

        protected override void OnCreate(Bundle bundle)
        {
            base.OnCreate(bundle);
            SetContentView(Resource.Layout.Main);

            serviceIntent = new Intent(this,typeof(MyService));
            serviceConnection = new MyServiceConnection();

            startServiceButton = FindViewById<Button>(Resource.Id.startService);
            startServiceButton.Click += delegate { 
                serviceIntent.PutExtra(MyService.STOP_THREAD,false);
                StartService(serviceIntent);
            };

            stopServiceButton = FindViewById<Button>(Resource.Id.stopService);
            stopServiceButton.Click += delegate {                 
                StopService(serviceIntent);
            };
        }

        protected override void OnResume()
        {
            if (serviceIntent != null && serviceConnection != null)
            {
                BindService(serviceIntent, serviceConnection, 0);
            }
            
            base.OnResume();
        }

        protected override void OnPause()
        {
            if (serviceConnection != null)
            {
                UnbindService(serviceConnection);
            }

            base.OnResume();
        }
    }
}

